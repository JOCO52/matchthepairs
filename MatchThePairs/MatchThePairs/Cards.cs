﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;


namespace MatchThePairs
{
    class Cards
    {
        //------------------------------
        //Data
        //------------------------------
        Texture2D image = null;
        Vector2 position = Vector2.Zero;
        bool flipped = false;
        bool paired = false;
        species cardimg = species.CHICKEN;
        Texture2D card = null;


        public enum species
        {
            CROC, // = 0
            CHICKEN, // = 1
            GORILLA, // = 2
            PANDA, // = 3
            BEAR, // = 4

        }
        //----------------------
        //behaviour
        //----------------------

        public void LoadContent(ContentManager content)
        {
            switch (cardimg)
            {
                case species.CROC:
                    image = content.Load<Texture2D>("graphics/crocodile");
                    break;

                case species.CHICKEN:
                    image = content.Load<Texture2D>("graphics/chicken");
                    break;

                case species.GORILLA:
                    image = content.Load<Texture2D>("graphics/gorilla");
                    break;

                case species.PANDA:
                    image = content.Load<Texture2D>("graphics/panda");
                    break;

                case species.BEAR:
                    image = content.Load<Texture2D>("graphics/bear");
                    break;


            }

            card = content.Load<Texture2D>("graphics/card");



        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(image, position, Color.White);
        }
    }
}
